//============================================================================
// Name        : pc3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "prob01/prob01.hpp"
#include "prob02/prob02.hpp"
#include "prob03/prob03.hpp"
#include "prob04/prob04.hpp"

using namespace std;

int main() {
	//cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	string problema = "2";

	prob01();
	prob02();
	prob03();
	prob04();

	return 0;
}
