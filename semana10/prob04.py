def es_perfecto(n):
    sumdiv = 0
    for i in range(1, n):
        if(n % i == 0):
            sumdiv = sumdiv + i
    if(sumdiv == n):
        return True
    else:
        return False
    
def obtener_perfectos(a, b):
    lista = []
    for i in range(a, b + 1):
        if(es_perfecto(i)):
            lista.append(i)
    return lista

#Pruebas
lista_per = obtener_perfectos(1, 500)
print(lista_per[1])






