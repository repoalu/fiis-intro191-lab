def leer_datos():
    continuar = True
    lista = []
    while(continuar == True):
        valor = int(input("Ingrese valor:"))
        if(valor == 0):
            continuar = False
        else:
            lista.append(valor)
    return lista

def obtener_promedio(lista):
    n = len(lista)
    suma = 0
    for i in range(n):
        suma = suma + lista[i]
    return suma / n    
        
#Pruebas
lista = leer_datos()
prom = obtener_promedio(lista) 
print(prom)


