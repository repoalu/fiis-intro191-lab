def mostrar_accion(producto, dias):
    if(producto == 120):
        print("Enviar a Agencia")
    else:
        if(dias >= 1 and dias <= 30):
            print("Mensaje de texto")
        elif(dias > 30 and dias <= 60):
            print("Llamada telefonica")
        elif(dias > 60 and dias <= 90):
            print("Visita cliente")
        else:
            print("Enviar a Agencia")
#Casos de prueba
mostrar_accion(110, 12)
mostrar_accion(120, 60)
mostrar_accion(110, 30.5)
